<?php 
use yii\grid\GridView;
use yii\data\ArrayDataProvider;
use yii\grid\StatusDataColumn;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;
use app\models\Profile;
use app\models\History;



 $this->title = Yii::t('user', 'Управління балансом');
 $this->params['breadcrumbs'][] = $this->title; ?>
<?= $this->render('/_alert', ['module' => Yii::$app->getModule('user')]) ?>
<?= $this->render('/admin/_menu') ?>


<?php $dataProvider = new ArrayDataProvider([
	'allModels' => $balance,
	'pagination' => [
		'pageSize' => 10,
	]
]);

?>

<?php //Pjax::begin(['id' => 'items', 'enablePushState' => false]) ?>

<?php echo GridView::widget([
	'dataProvider' => $dataProvider,
	'showHeader' => true,
    'layout'       => "{items}\n{pager}",
	'columns' => [
		[
			'attribute' => 'FullName',
			'label' => 'ПІБ'
		],
		[
			'attribute' => 'UserName',
			'label' => 'Логін'
		],
		[
			'attribute' => 'Email',
			'label' => 'Пошта'
		],
		[
			'attribute' => 'Balance',
			'label' => 'Баланс'
		],
	
		[
            'header' => 'Редагування балансу',
            'value' => function ($model) {
                    return "<a class=\"btn btn-sm btn-success givemoney\" data-user-id=\"" .$model['UserID']. "\">Редагувати баланс</a>"; // \"" . $balance['UserID'] . "\"     
                    	  // \"" . $model->id . "\"

            },
            'format' => 'raw',
        ],
        [
        	'class' => 'yii\grid\ActionColumn',
            'template' => '{resend_password} {delete}',
            'buttons' => [
                'resend_password' => function ($url, $model, $key) {
                    if (!$model->isAdmin) {
                        return '
                    <a data-method="POST" data-confirm="' . Yii::t('user', 'Are you sure?') . '" href="' . Url::to(['resend-password', 'id' => $model['UserID']]) . '">
                    <span title="' . Yii::t('user', 'Generate and send new password to user') . '" class="glyphicon glyphicon-envelope">
                    </span> </a>';
                    }
                },
            ]
        ],
    ],
]); ?>

<?php //Pjax::end() ?>

<?php yii\bootstrap\Modal::begin(['id'=>'bModal','header' => '<h3>Редагування балансу</h3>', 'size' => 'modal-sm']); ?>
<?= $this->render('_balance_form'); ?>
<?php yii\bootstrap\Modal::end();?>


<?php
$this->registerJs("function onReadyAndPjaxSuccess() {
    $('.givemoney').click(function(e) {
               e.preventDefault();
               $('#bModal').modal('show').find('.modal-content').load($(this).attr('href'));
               var user_id = $(this).attr('data-user-id');
                $('#bModal').attr('data-user-id', user_id);
    
            });
};

$('#bModal').on('givemoneyconfirm', function (e, obj) {
    console.log('executed');
        $.ajax({
            url:'/user/admin/money?id=' + obj.userId + '&summ=' + obj.summ,
            success: function(result) {
                $.pjax.reload({container:'[id=items]'}); 
            },
            error: function() {
            }
        });
    });

$(document).on('ready',onReadyAndPjaxSuccess);
$(document).on('pjax:success',onReadyAndPjaxSuccess);
 ");
?>